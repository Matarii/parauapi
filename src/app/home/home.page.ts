import { Component, OnInit } from '@angular/core';
import { ApiService } from "../services/api/api.service";
import { DomSanitizer } from '@angular/platform-browser';
import { Article } from '../models/article.model';
import { Router, NavigationExtras } from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage implements OnInit{

  topHeadlines: Article[];
  slideOpts = {
    initialSlide: 1,
    speed: 400
  };



  constructor(
    private api: ApiService,
    private router: Router, 
    private sanitizer: DomSanitizer) 
  { }

  ngOnInit() {
    this.getTopHealines();

  }


  getSafeStyle(url: string) {
    return this.sanitizer.bypassSecurityTrustStyle('url(' + url + ')');
  }

  getTopHealines() {
    this.api.get('search?query=language%3AFR&sortBy=_score&sortOrder=DESC&limit=15').subscribe((data: Article[])=>{
      console.log('getTopHeadlines',data);
      this.topHeadlines = data;
    })
  }

  getDetail(index) {
    let data: NavigationExtras = {
      state: {
        article: this.topHeadlines[index]
      }
    };
    this.router.navigate(['detail'], data);
  }
}

