export interface Article {
    score: number,
    text: string,
    discoverDate: string,
    website: {
        name: string,
        hostName: string,
    }
    title: string,
    url: string,
    elements: {url: string},
}
