import { Article } from './article.model';

export interface Reponse {
    response: Article[],
    status: string,
    totalResults: string,
}
