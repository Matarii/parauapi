import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
  selector: 'app-detail',
  templateUrl: './detail.page.html',
  styleUrls: ['./detail.page.scss'],
})
export class DetailPage implements OnInit {

  article: any;
 
  constructor(private route: ActivatedRoute, private router: Router, private sanitizer: DomSanitizer) {
    this.route.queryParams.subscribe(params => {
      if (this.router.getCurrentNavigation().extras.state) {
        this.article = this.router.getCurrentNavigation().extras.state.article;
        console.log('article', this.article);
      } else {
        this.router.navigate(['home']);
      }
    });
  }

  ngOnInit() {
  }



  getSafeStyle(url: string) {
    return this.sanitizer.bypassSecurityTrustStyle('url(' + url + ')');
  }

}
