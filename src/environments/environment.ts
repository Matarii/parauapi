// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

// export const environment = {
//   production: false,
//   apiKey: 'e2bf807c24d14dfaa654cf33b3065f9b',
//   apiUrl: 'https://newsapi.org/v2/',
// };

export const environment = {
  production: false,
  apiKey: 'sBBqsGXiYgF0Db5OV5tAw_F7gcXxQfvFrHOVvN6Xl1v-L6fvfcQ_okCq484fktX8n2pHZrSf1gT2PUujH1YaQA',
  apiUrl: 'https://api.newsriver.io/v2/',
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
